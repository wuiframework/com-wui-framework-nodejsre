# com-wui-framework-nodejsre v2019.1.0

> Runtime environment for back-end projects based on custom Node.js build.

## Requirements

This library does not have any special requirements, but it depends on the 
[WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder). See the WUI Builder requirements before you build this project.

## Project build

The project build is fully automated. For more information about the project build, see the 
[WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder) documentation.

## History

### v2019.1.0
Usage of resource writer instead of simple stream-based implementation.
### v2019.0.2
Libelf replaced by stream-based resources. Added full support for OSX. Added ability to pass externally created resources map.
### v2019.0.1
Added support for cross-compilation to aarch64. Upgrade of NodeJS to 10.15 LTS. Updated configuration to produce node as shared library.
Integrated http/https redirection to enable patching feature for native node module build based on NodejsRE distribution
instead of official one (change from node.lib=>node.exe to node.lib=>node.dll).
### v2019.0.0
Added more known file extensions. Added wrapper for fileRead with async API.
### v2018.3.0
Conversion of the project into the code based for custom Nodejs builds. Initial implementation of embedded resources loader.
### v1.1.0
Update of Nodejs version and tests. SCR update and change of history ordering.
### v1.0.1
Convert config files from XML to JSONP. Update of Commons lib.
### v1.0.0
Initial release.

## License

This software is owned or controlled by NXP Semiconductors. 
The use of this software is governed by the BSD-3-Clause Licence distributed with this material.
  
See the `LICENSE.txt` file for more details.

---

Author Jakub Cieslar, 
Copyright (c) 2017-2019 [NXP](http://nxp.com/)
