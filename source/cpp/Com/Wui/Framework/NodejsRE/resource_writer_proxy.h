/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef SRC_RESOURCE_WRITER_PROXY_H_
#define SRC_RESOURCE_WRITER_PROXY_H_

#if defined(__linux__) || defined(__APPLE__)
#   include <ResourceWriter/ResourceWriter.hpp>
namespace ResourceWriter = ::Com::Wui::Framework::ResourceWriter;
#endif

#endif  // SRC_RESOURCE_WRITER_PROXY_H_
