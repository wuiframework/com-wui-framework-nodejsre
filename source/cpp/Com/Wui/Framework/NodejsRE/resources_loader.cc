/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include <node_buffer.h>

#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

HMODULE hmodule = nullptr;
#endif

#include "resources_loader.h"

namespace node {
    using std::string;
    using v8::Context;
    using v8::FunctionCallbackInfo;
    using v8::HandleScope;
    using v8::Isolate;
    using v8::Local;
    using v8::Object;
    using v8::String;
    using v8::Value;

#ifndef WIN32
    ResourceWriter::Reader::ResourceMap ResourcesLoader::cachedResources;
#endif

    void ResourcesLoader::Initialize(Local <Object> $target, Local <Value> $unused, Local <Context> $context) {
        Environment *env = Environment::GetCurrent($context);
        env->SetMethod($target, "read", Read);
        env->SetMethod($target, "write", Write);
        env->SetMethod($target, "update", Update);
        env->SetMethod($target, "updateOrWrite", UpdateOrWrite);

        env->SetMethod($target, "cacheResources", CacheResources);
        env->SetMethod($target, "clearCachedResources", ClearCachedResources);
        env->SetMethod($target, "readCached", ReadCached);
    }

    void ResourcesLoader::CacheResources(const v8::FunctionCallbackInfo <v8::Value> &$args) {
#ifndef WIN32
        Isolate *isolate = Isolate::GetCurrent();
        Local <Context> context = isolate->GetCurrentContext();
        HandleScope scope(isolate);
        const std::string path = string(*String::Utf8Value(isolate, $args[0]));
        Local <v8::Function> logCallback;
        bool jsLogCallbackAvailable = false;

        if ($args.Length() == 2) {
            jsLogCallbackAvailable = true;
            logCallback = Local<v8::Function>::Cast($args[1]);
        }

        if (jsLogCallbackAvailable) {
            ResourcesLoader::setUpLoggingToJavascript(logCallback, isolate, context);
        } else {
            ResourcesLoader::disableLogging();
        }

        try {
            ResourceWriter::Reader::Settings settings{path};
            ResourceWriter::Reader::ResourceReader resourceReader{std::move(settings)};
            ResourcesLoader::cachedResources = resourceReader.Read();

            if (ResourcesLoader::cachedResources.empty()) {
                isolate->ThrowException(v8::Exception::TypeError(v8::String::NewFromUtf8(isolate, "Could not find any resources")));
            }
        } catch (const std::exception &exception) {
            isolate->ThrowException(v8::Exception::Error(v8::String::NewFromUtf8(isolate, exception.what())));
        } catch (...) {
            isolate->ThrowException(v8::Exception::Error(v8::String::NewFromUtf8(isolate, "Unknown error")));
        }
#endif
    }

    void ResourcesLoader::ClearCachedResources(const v8::FunctionCallbackInfo <v8::Value> &/*$args*/) {
#ifndef WIN32
        ResourcesLoader::cachedResources.clear();
#endif
    }

    void ResourcesLoader::ReadCached(const v8::FunctionCallbackInfo <v8::Value> &$args) {
#ifdef WIN32
        ResourcesLoader::Read($args);
#else
        Isolate *isolate = Isolate::GetCurrent();
        Local <Context> context = isolate->GetCurrentContext();
        HandleScope scope(isolate);
        bool jsLogCallbackAvailable = false;
        Local <v8::Function> logCallback;
        const string group = string(*String::Utf8Value(isolate, $args[0]));
        const string id = string(*String::Utf8Value(isolate, $args[1]));
        const string resourceId = "." + group + "/" + id;

        if ($args.Length() == 4) {
            jsLogCallbackAvailable = true;
            logCallback = Local<v8::Function>::Cast($args[3]);
        }

        if (jsLogCallbackAvailable) {
            ResourcesLoader::setUpLoggingToJavascript(logCallback, isolate, context);
        } else {
            ResourcesLoader::disableLogging();
        }

        const auto resource = ResourcesLoader::cachedResources.find(resourceId);

        if (resource != std::cend(ResourcesLoader::cachedResources)) {
            const auto rawData = resource->second.getData();
            const auto size = rawData.size();
            const char *bufPtr = reinterpret_cast<const char *>(&rawData[0]);
            v8::Local <v8::Object> buf;

            if (node::Buffer::Copy(isolate, bufPtr, size).ToLocal(&buf)) {
                $args.GetReturnValue().Set(buf);
            } else {
                isolate->ThrowException(
                        v8::Exception::TypeError(v8::String::NewFromUtf8(isolate, "Cannot create buffer from resource")));
            }
        } else {
            isolate->ThrowException(v8::Exception::Error(v8::String::NewFromUtf8(isolate, "Resource not found")));
        }
#endif
    }

    void ResourcesLoader::Read(const FunctionCallbackInfo <Value> &$args) {
        Isolate *isolate = Isolate::GetCurrent();
        Local <Context> context = isolate->GetCurrentContext();
        HandleScope scope(isolate);
        bool jsLogCallbackAvailable = false;

        if ($args.Length() == 3 || $args.Length() == 4) {
            if ($args[0]->IsString() && $args[1]->IsString() && $args[2]->IsString()) {
                Local <v8::Function> logCallback;
                const string group = string(*String::Utf8Value(isolate, $args[0]));
                const string id = string(*String::Utf8Value(isolate, $args[1]));
                const string path = string(*String::Utf8Value(isolate, $args[2]));
                const string resourceId = group + "/" + id;

                if ($args.Length() == 4) {
                    jsLogCallbackAvailable = true;
                    logCallback = Local<v8::Function>::Cast($args[3]);
                }
#ifdef WIN32
                if (path != "" && hmodule == nullptr) {
                    hmodule = LoadLibrary(path.c_str());
                    if (hmodule == nullptr) {
                        isolate->ThrowException(v8::Exception::TypeError(
                                v8::String::NewFromUtf8(isolate, "Could not load exe")));
                        return;
                    }
                }

                HRSRC hrsrc = FindResource(hmodule, id.c_str(), group.c_str());
                if (hrsrc != nullptr) {
                    HGLOBAL hglobal = LoadResource(hmodule, hrsrc);
                    if (hglobal != nullptr) {
                        auto *bufPtr = static_cast<char *>(LockResource(hglobal));
                        if (bufPtr != nullptr) {
                            auto size = static_cast<unsigned int>(SizeofResource(hmodule, hrsrc));
                            v8::Local <v8::Object> buf;
                            if (node::Buffer::Copy(isolate, bufPtr, size).ToLocal(&buf)) {
                                $args.GetReturnValue().Set(buf);
                            } else {
                                isolate->ThrowException(
                                        v8::Exception::TypeError(v8::String::NewFromUtf8(
                                                isolate, "Cannot create buffer from resource")));
                            }
                        } else {
                            isolate->ThrowException(
                                    v8::Exception::TypeError(v8::String::NewFromUtf8(
                                            isolate, "Locked resource points to NULL for resource")));
                        }
                    } else {
                        isolate->ThrowException(v8::Exception::TypeError(
                                v8::String::NewFromUtf8(isolate, "Can not load resource")));
                    }
                } else {
                    isolate->ThrowException(v8::Exception::Error(
                            v8::String::NewFromUtf8(isolate, "Can not find resource")));
                }
#else
                try {
                    if (jsLogCallbackAvailable) {
                        ResourcesLoader::setUpLoggingToJavascript(logCallback, isolate, context);
                    } else {
                        ResourcesLoader::disableLogging();
                    }

                    ResourceWriter::Reader::Settings settings{path};
                    const auto resourceReader = std::make_unique<ResourceWriter::Reader::ResourceReader>(std::move(settings));
                    const auto extractedResources = resourceReader->Read();

                    const auto resource = extractedResources.find(resourceId);

                    if (resource != std::cend(ResourcesLoader::cachedResources)) {
                        const auto rawData = resource->second.getData();
                        const auto size = rawData.size();
                        const char *bufPtr = reinterpret_cast<const char *>(&rawData[0]);
                        v8::Local <v8::Object> buf;

                        if (node::Buffer::Copy(isolate, bufPtr, size).ToLocal(&buf)) {
                            $args.GetReturnValue().Set(buf);
                        } else {
                            isolate->ThrowException(
                                    v8::Exception::TypeError(v8::String::NewFromUtf8(isolate, "Cannot create buffer from resource")));
                        }
                    } else {
                        isolate->ThrowException(v8::Exception::Error(v8::String::NewFromUtf8(isolate, "Can not find resource")));
                    }
                } catch (const std::exception &exception) {
                    isolate->ThrowException(v8::Exception::Error(v8::String::NewFromUtf8(isolate, exception.what())));
                } catch (...) {
                    isolate->ThrowException(v8::Exception::Error(v8::String::NewFromUtf8(isolate, "Unknown error")));
                }
#endif
            } else {
                isolate->ThrowException(v8::Exception::TypeError(String::NewFromUtf8(isolate, "Group, id, path have to be strings")));
            }
        } else {
            isolate->ThrowException(v8::Exception::SyntaxError(String::NewFromUtf8(
                    isolate, "Incorrect count of arguments, 3 or 4 expected")));
        }
    }

    void ResourcesLoader::Write(const v8::FunctionCallbackInfo <v8::Value> &$args) {
        ResourcesLoader::injectResource($args, WriteOperationType::WRITE);
    }

    void ResourcesLoader::Update(const v8::FunctionCallbackInfo <v8::Value> &$args) {
        ResourcesLoader::injectResource($args, WriteOperationType::UPDATE);
    }

    void ResourcesLoader::UpdateOrWrite(const v8::FunctionCallbackInfo <v8::Value> &$args) {
        ResourcesLoader::injectResource($args, WriteOperationType::UPDATE_OR_WRITE);
    }

    bool ResourcesLoader::HasEmbeddedResources(const std::string &$path) {
        bool hasEmbeddedResources = false;

#ifdef WIN32
        if ($path != "" && hmodule == nullptr) {
            hmodule = LoadLibrary($path.c_str());
            if (hmodule != nullptr) {
                HRSRC hrsrc = FindResource(hmodule, "PACKAGE_MAP", "CONFIG");
                if (hrsrc != nullptr) {
                    HGLOBAL hglobal = LoadResource(hmodule, hrsrc);
                    if (hglobal != nullptr) {
                        auto *bufPtr = static_cast<char *>(LockResource(hglobal));
                        if (bufPtr != nullptr) {
                            auto size = static_cast<unsigned int>(SizeofResource(hmodule, hrsrc));
                            hasEmbeddedResources = true;
                        }
                    }
                }
            }
        }
#else
        try {
            ResourcesLoader::disableLogging();

            ResourceWriter::Reader::Settings settings{$path};
            ResourceWriter::Reader::ResourceReader resourceReader{std::move(settings)};
            const auto extractedResources = resourceReader.Read();

            const std::string resourceId = ".CONFIG/PACKAGE_MAP";

            hasEmbeddedResources = (extractedResources.find(resourceId) != std::cend(extractedResources));
        } catch (...) {
            // suppress every exception and don't output anything (since the standard output and error are being parsed by the nodejs),
            // if something goes wrong, there are no embedded resources
        }

#endif
        return hasEmbeddedResources;
    }

    void ResourcesLoader::injectResource(const v8::FunctionCallbackInfo <v8::Value> &$args, WriteOperationType $type) {
#ifndef WIN32
        Isolate *isolate = Isolate::GetCurrent();
        Local <Context> context = isolate->GetCurrentContext();
        HandleScope scope(isolate);
        bool jsLogCallbackAvailable = false;

        if ($args.Length() == 4 || $args.Length() == 5) {
            if ($args[0]->IsString() && $args[1]->IsString() && $args[2]->IsString() && $args[3]->IsString()) {
                Local <v8::Function> logCallback;
                const string group = string(*String::Utf8Value(isolate, $args[0]));
                const string id = string(*String::Utf8Value(isolate, $args[1]));
                const string path = string(*String::Utf8Value(isolate, $args[2]));
                const string pathToResource = string(*String::Utf8Value(isolate, $args[3]));
                string resourceId = group + "/" + id;

                if ($args.Length() == 5) {
                    jsLogCallbackAvailable = true;
                    logCallback = Local<v8::Function>::Cast($args[4]);
                }

                try {
                    if (jsLogCallbackAvailable) {
                        ResourcesLoader::setUpLoggingToJavascript(logCallback, isolate, context);
                    } else {
                        ResourcesLoader::disableLogging();
                    }

                    ResourceWriter::Writer::Settings settings{path};
                    ResourceWriter::Writer::ResourceWriter resourceWriter{std::move(settings)};
                    ResourceWriter::Writer::Resource resource{pathToResource};

                    if ($type == WriteOperationType::WRITE) {
                        resourceWriter.Write({{std::move(resourceId), std::move(resource)}});
                    } else if ($type == WriteOperationType::UPDATE) {
                        resourceWriter.Update({{std::move(resourceId), std::move(resource)}});
                    } else if ($type == WriteOperationType::UPDATE_OR_WRITE) {
                        resourceWriter.UpdateOrWrite({{std::move(resourceId), std::move(resource)}});
                    }
                } catch (const std::exception &exception) {
                    isolate->ThrowException(v8::Exception::Error(v8::String::NewFromUtf8(isolate, exception.what())));
                } catch (...) {
                    isolate->ThrowException(v8::Exception::Error(v8::String::NewFromUtf8(isolate, "Unknown error")));
                }
            } else {
                isolate->ThrowException(v8::Exception::TypeError(String::NewFromUtf8(
                        isolate, "Group, id, path, path-to-resource have to be strings")));
            }
        } else {
            isolate->ThrowException(v8::Exception::SyntaxError(String::NewFromUtf8(
                    isolate, "Incorrect count of arguments, 4 or 5 expected")));
        }
#endif
    }

    void ResourcesLoader::setUpLoggingToJavascript(v8::Local <v8::Function> $callback, v8::Isolate *$isolate, v8::Local <v8::Context> &$context) {
#ifndef WIN32
        const auto invokeJsCallback = [$context, $isolate, $callback](std::string &&$message, std::string &&$verbosity) {
            Local <Value> argv[2];
            argv[0] = {v8::String::NewFromUtf8($isolate, $message.c_str(), v8::NewStringType::kNormal).ToLocalChecked()};
            argv[1] = {v8::String::NewFromUtf8($isolate, $verbosity.c_str(), v8::NewStringType::kNormal).ToLocalChecked()};
            $callback->Call($context, Null($isolate), 2, argv).ToLocalChecked();
        };

        auto loggerErrorCallback = [invokeJsCallback](std::string &&$message) {
            invokeJsCallback(std::move($message), "error");
        };

        auto loggerInfoCallback = [invokeJsCallback](std::string &&$message) {
            invokeJsCallback(std::move($message), "info");
        };

        auto loggerDebugCallback = [invokeJsCallback](std::string &&$message) {
            invokeJsCallback(std::move($message), "debug");
        };

        ResourceWriter::Logger::Logger logger{std::move(loggerErrorCallback), std::move(loggerInfoCallback),
                                              std::move(loggerDebugCallback)};
        ResourceWriter::Logger::LogManager::setLogger(std::move(logger));
#endif
    }

    void ResourcesLoader::disableLogging() {
#ifndef WIN32
        ResourceWriter::Logger::LogManager::setLogger(ResourceWriter::Logger::LoggerDevNull{});
#endif
    }
}  // namespace node

NODE_BUILTIN_MODULE_CONTEXT_AWARE(resources_loader,
        node::ResourcesLoader::Initialize
)