/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef SRC_RESOURCES_LOADER_H_
#define SRC_RESOURCES_LOADER_H_

#if defined(NODE_WANT_INTERNALS) && NODE_WANT_INTERNALS

#include <string>

#include "node_internals.h"

#include "resource_writer_proxy.h"

namespace node {
    class ResourcesLoader {
     public:
        static void Initialize(v8::Local <v8::Object> $target, v8::Local <v8::Value> $unused, v8::Local <v8::Context> $context);

        /**
         * Arguments when called from the js:
         * path <string>, log js callback <function, optional>
         */
        static void CacheResources(const v8::FunctionCallbackInfo <v8::Value> &$args);

        /**
         * Arguments when called from the js:
         * (none)
         */
        static void ClearCachedResources(const v8::FunctionCallbackInfo <v8::Value> &$args);

        /**
         * Arguments when called from the js are same as for "Read"
         */
        static void ReadCached(const v8::FunctionCallbackInfo <v8::Value> &$args);

        /**
         * Arguments when called from the js:
         * group <string>, id <string>, path <string>, log js callback <function, optional>
         */
        static void Read(const v8::FunctionCallbackInfo <v8::Value> &$args);

        /**
         * Arguments when called from the js:
         * group <string>, id <string>, path <string>, path-to-resource <string>, log js callback <function, optional>
         */
        static void Write(const v8::FunctionCallbackInfo <v8::Value> &$args);

        /**
         * Arguments when called from the js are same as for "Write"
         */
        static void Update(const v8::FunctionCallbackInfo <v8::Value> &$args);

        /**
         * Arguments when called from the js are same as for "Write"
         */
        static void UpdateOrWrite(const v8::FunctionCallbackInfo <v8::Value> &$args);

        /**
         * Arguments when called from the js:
         * (none)
         */
        static bool HasEmbeddedResources(const std::string &$path);

     private:
        enum class WriteOperationType : unsigned char {
            WRITE = 0,
            UPDATE,
            UPDATE_OR_WRITE
        };

        static void injectResource(const v8::FunctionCallbackInfo <v8::Value> &$args, WriteOperationType $type);

        static void setUpLoggingToJavascript(v8::Local <v8::Function> $callback, v8::Isolate *$isolate, v8::Local<v8::Context> &$context);

        static void disableLogging();

#ifndef WIN32
        static ResourceWriter::Reader::ResourceMap cachedResources;
#endif
    };
}  // namespace node

#endif  // defined(NODE_WANT_INTERNALS) && NODE_WANT_INTERNALS

#endif  // SRC_RESOURCES_LOADER_H_
