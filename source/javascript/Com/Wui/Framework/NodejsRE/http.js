// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.

/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

'use strict';

const {Agent, globalAgent} = require('_http_agent');
const {ClientRequest} = require('_http_client');
const {methods} = require('_http_common');
const {IncomingMessage} = require('_http_incoming');
const {OutgoingMessage} = require('_http_outgoing');
const {
    _connectionListener,
    STATUS_CODES,
    Server,
    ServerResponse
} = require('_http_server');
let maxHeaderSize;

function createServer(opts, requestListener) {
    return new Server(opts, requestListener);
}

function request(url, options, cb) {
    let switchToHttps = false;
    const checkPatch = function ($url) {
        try {
            const patchMap = JSON.parse(process.env.NODEJSRE_MOCK_SERVER_MAP);

            for (let patchMapKey in patchMap) {
                if (patchMap.hasOwnProperty(patchMapKey)) {
                    if (patchMapKey === $url) {
                        $url = patchMap[patchMapKey];
                        switchToHttps = $url.indexOf("https:") === 0;
                        break;
                    }
                }
            }
        } catch (e) {
            // failed, just continue without patch
        }

        return $url;
    };

    if (process.env.hasOwnProperty("NODEJSRE_MOCK_SERVER_MAP") && process.env.NODEJSRE_MOCK_SERVER_MAP.length > 0) {
        if (typeof url === 'string') {
            url = checkPatch(url);
        } else if (typeof url === 'object' && url.hasOwnProperty('href')) {
            url = checkPatch(url.href);
        }
    }

    if (!switchToHttps) {
        return new ClientRequest(url, options, cb);
    } else {
        return require("https").request(url, options, cb);
    }
}

function get(url, options, cb) {
    var req = request(url, options, cb);
    req.end();
    return req;
}

module.exports = {
    _connectionListener,
    METHODS: methods.slice().sort(),
    STATUS_CODES,
    Agent,
    ClientRequest,
    globalAgent,
    IncomingMessage,
    OutgoingMessage,
    Server,
    ServerResponse,
    createServer,
    get,
    request
};

Object.defineProperty(module.exports, 'maxHeaderSize', {
    configurable: true,
    enumerable: true,
    get() {
        if (maxHeaderSize === undefined) {
            const {getOptionValue} = require('internal/options');
            maxHeaderSize = getOptionValue('--max-http-header-size');
        }

        return maxHeaderSize;
    }
});
