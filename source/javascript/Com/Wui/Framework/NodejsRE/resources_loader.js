/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

const os = require("os");
const fs = require("fs");
const path = require("path");
const nativeModule = require("module");
const util = require("util");
const debug = util.debuglog("resources_loader");

const loader = process.binding("resources_loader");

let snapshotPath = "/snapshot";
const cwd = path.normalize(path.dirname(process.argv[0])).replace(/\\/g, "/");
debug("cwd: " + cwd + ", argv0: " + process.argv0);
let resourcesMap = {};

function logFromLoader($message, $verbosity) {
    debug($verbosity + ", " + $message);
}

function normalizePath($path) {
    $path = $path.replace(/\\/g, "/").replace(/\/\//g, "/");
    if (!resourcesMap.hasOwnProperty($path)) {
        if ($path.startsWith("/resource/")) {
            if (resourcesMap.hasOwnProperty(snapshotPath + $path)) {
                $path = snapshotPath + $path;
            } else {
                $path = cwd + $path;
            }
        }
        if ($path.indexOf(":" + snapshotPath) > 0) {
            $path = $path.substring(2);
        }
    }
    return $path;
}

function getStats($path) {
    debug("stats: " + $path);
    let record = {};
    if (resourcesMap.hasOwnProperty($path) && resourcesMap[$path].stats !== null) {
        record = JSON.parse(resourcesMap[$path].stats);
    }
    const now = new Date();
    return {
        isFile: function () {
            if (record.hasOwnProperty("isFile")) {
                return record.isFile;
            }
            return true;
        },
        isDirectory: function () {
            if (record.hasOwnProperty("isDirectory")) {
                return record.isDirectory;
            }
            return false;
        },
        isBlockDevice: function () {
            if (record.hasOwnProperty("isBlockDevice")) {
                return record.isBlockDevice;
            }
            return false;
        },
        isCharacterDevice: function () {
            if (record.hasOwnProperty("isCharacterDevice")) {
                return record.isCharacterDevice;
            }
            return false;
        },
        isSymbolicLink: function () {
            if (record.hasOwnProperty("isSymbolicLink")) {
                return record.isSymbolicLink;
            }
            return false;
        },
        isFIFO: function () {
            if (record.hasOwnProperty("isFIFO")) {
                return record.isFIFO;
            }
            return false;
        },
        isSocket: function () {
            if (record.hasOwnProperty("isSocket")) {
                return record.isSocket;
            }
            return false;
        },
        dev: 0,
        mode: 0,
        nlink: 1,
        uid: 0,
        gid: 0,
        rdev: 0,
        ino: 0,
        size: 0,
        atimeMs: 0,
        mtimeMs: 0,
        ctimeMs: 0,
        birthtimeMs: 0,
        atime: record.hasOwnProperty("atime") ? new Date(record.atime) : now,
        mtime: record.hasOwnProperty("mtime") ? new Date(record.mtime) : now,
        ctime: record.hasOwnProperty("ctime") ? new Date(record.ctime) : now,
        birthtime: record.hasOwnProperty("birthtime") ? new Date(record.birthtime) : now,
    };
}

function isEmbeddedFile($path) {
    if (resourcesMap.hasOwnProperty($path)) {
        return getStats($path).isFile();
    }
    return false;
}

module.exports = {
    read: loader.read,
    write: loader.write,
    update: loader.update,
    updateOrWrite: loader.updateOrWrite,
    cacheResources: loader.cacheResources,
    clearCachedResources: loader.clearCachedResources,
    readCached: loader.readCached,

    init: function ($resourcesMap, $snapshotPath) {
        debug("Initializing resources loader");
        let isEmbedded = true;
        if ($resourcesMap !== undefined) {
            debug("Force used resources map.");
            resourcesMap = $resourcesMap;
            if ($snapshotPath !== undefined) {
                snapshotPath = $snapshotPath;
            }
        } else {
            try {
                loader.cacheResources(process.argv0, logFromLoader);
                debug("All resources are cached");
                const packageMapData = loader.readCached("CONFIG", "PACKAGE_MAP", process.argv0, logFromLoader);
                if (packageMapData !== null) {
                    resourcesMap = JSON.parse(packageMapData.toString());
                } else {
                    isEmbedded = false;
                }
            } catch (ex) {
                debug("Failed to load resources map. Continuing in process without embedded resources.", ex);
                isEmbedded = false;
            }
        }
        if (isEmbedded) {
            process.env.NODE_PATH = snapshotPath + "/node_modules";
            const existsSync = fs.existsSync;
            fs.existsSync = function ($path, $options) {
                if ($path === snapshotPath + "/node_modules") {
                    return true;
                }
                $path = normalizePath($path);
                debug("exists: " + $path);
                if (resourcesMap.hasOwnProperty($path)) {

                    return true;
                }
                return existsSync($path, $options);
            };

            const statSync = fs.statSync;
            fs.statSync = function ($path, $options) {
                $path = normalizePath($path);
                if (resourcesMap.hasOwnProperty($path)) {
                    return getStats($path);
                }
                return statSync($path, $options);
            };
            const lstatSync = fs.lstatSync;
            fs.lstatSync = function ($path, $options) {
                $path = normalizePath($path);
                if (resourcesMap.hasOwnProperty($path)) {
                    return getStats($path);
                }
                return lstatSync($path, $options);
            };

            const stringExtensions = [
                ".js", ".json", ".jsonp", ".ts",
                ".txt", ".xml", ".conf", ".yml", ".tpl",
                ".java", ".e4xmi", ".MF",
                ".html", ".css", ".map",
                ".cmd", ".sh",
                ".cpp", ".hpp", ".c", ".h", ".cmake",
                ".key", ".crt", ".pem"
            ];
            const readFileSync = fs.readFileSync;
            fs.readFileSync = function ($path, $options) {
                $path = normalizePath($path);
                debug("read: " + $path);
                if (resourcesMap.hasOwnProperty($path)) {
                    try {
                        const buffer = loader.readCached("RESOURCES", resourcesMap[$path].name, process.argv0, logFromLoader);
                        if ($options === undefined) {
                            $options = "utf8";
                        }
                        if (stringExtensions.indexOf(path.extname($path)) !== -1) {
                            return buffer.toString($options);
                        }
                        return buffer;
                    } catch (ex) {
                        // fallback to native processing
                    }
                }
                return readFileSync($path, $options);
            };

            const readFile = fs.readFile;
            fs.readFile = function ($path, $options, $callback) {
                $path = normalizePath($path);
                debug("read async: " + $path);
                if (resourcesMap.hasOwnProperty($path)) {
                    try {
                        if ($callback === undefined) {
                            $callback = $options;
                            $options = undefined;
                        }
                        $callback(null, fs.readFileSync($path, $options));
                    } catch (ex) {
                        $callback(ex);
                    }
                } else {
                    readFile($path, $options, $callback);
                }
            };

            const readdirSync = fs.readdirSync;
            fs.readdirSync = function ($path, $options) {
                $path = normalizePath($path);
                debug("readdir: " + $path);
                if (resourcesMap.hasOwnProperty($path) && getStats($path).isDirectory()) {
                    const content = [];
                    let key;
                    for (key in resourcesMap) {
                        if (key !== $path && resourcesMap.hasOwnProperty(key) && key.indexOf($path) !== -1) {
                            key = key.replace($path + "/", "");
                            if (key.indexOf("/") === -1) {
                                content.push(key);
                            }
                        }
                    }
                    return content;
                }
                return readdirSync($path, $options);
            };

            const createReadStream = fs.createReadStream;
            fs.createReadStream = function ($path, $options) {
                $path = normalizePath($path);
                debug("stream: " + $path);
                if (resourcesMap.hasOwnProperty($path)) {
                    const Readable = require("stream").Readable;
                    const stream = new Readable();
                    stream._read = () => {
                        // mock _read
                    };
                    stream.push(fs.readFileSync($path));
                    stream.push(null);
                    return stream;
                }
                return createReadStream($path, $options);
            };

            const copyFileSync = fs.copyFileSync;
            fs.copyFileSync = function ($src, $dest, $flags) {
                $src = normalizePath($src);
                debug("copyfile: '" + $src + "' -> '" + $dest + "'");
                if (resourcesMap.hasOwnProperty($src)) {
                    fs.writeFileSync($dest, loader.readCached("RESOURCES", resourcesMap[$src].name, process.argv0, logFromLoader));
                } else {
                    copyFileSync($src, $dest, $flags);
                }
            };

            const _findPath = nativeModule._findPath;
            nativeModule._findPath = function ($request, $paths, $isMain) {
                let fileName = _findPath($request, $paths, $isMain);

                const extension = Object.keys(nativeModule._extensions);
                const tryExtensions = function ($path) {
                    for (let index = 0; index < extension.length; index++) {
                        let path = $path + extension[index];
                        debug("findPath: " + path);
                        if (isEmbeddedFile(path)) {
                            $path = path;
                            fileName = path;
                            break;
                        }
                    }
                    return $path;
                };
                if (!fileName) {
                    for (let index = 0; index < $paths.length; index++) {
                        let curPath = normalizePath(path.resolve($paths[index], $request));
                        debug("findPath: " + curPath);
                        if (isEmbeddedFile(curPath)) {
                            fileName = curPath;
                        } else {
                            curPath = tryExtensions(curPath);
                            if (!fileName && curPath.indexOf(".node") !== -1 && curPath.indexOf("/resource/libs") !== -1) {
                                fileName = process.nodejsRoot + curPath.substr(curPath.indexOf("/resource/libs"));
                            }
                        }
                        if (!fileName) {
                            const jsonPath = curPath + "/package.json";
                            debug("findPath: " + jsonPath);
                            if (isEmbeddedFile(jsonPath)) {
                                const json = fs.readFileSync(jsonPath);
                                if (json !== undefined && json !== null) {
                                    try {
                                        const packageJson = JSON.parse(json);
                                        if (packageJson.hasOwnProperty("main")) {
                                            const mainScript = packageJson.main;
                                            if (mainScript !== undefined) {
                                                const main = normalizePath(path.resolve(curPath, mainScript));
                                                if (isEmbeddedFile(main)) {
                                                    fileName = main;
                                                }
                                                if (!fileName) {
                                                    tryExtensions(main);
                                                }
                                                if (!fileName) {
                                                    tryExtensions(main + "/index");
                                                }
                                            }
                                        }
                                    } catch (ex) {
                                        ex.path = jsonPath;
                                        ex.message = "Error parsing " + jsonPath + ": " + ex.message;
                                        throw ex;
                                    }
                                }
                            }
                        } else {
                            break;
                        }
                        if (!fileName) {
                            tryExtensions(curPath + "/index");
                        } else {
                            break;
                        }
                    }
                    if (fileName !== false) {
                        const cacheKey = $request + "\x00" + ($paths.length === 1 ? $paths[0] : $paths.join("\x00"));
                        nativeModule._pathCache[cacheKey] = fileName;
                    }
                }
                return fileName;
            };

            const _resolveFilename = nativeModule._resolveFilename;
            nativeModule._resolveFilename = function ($request, $parent, $isMain, $options) {
                let filename;
                try {
                    filename = _resolveFilename($request, $parent, $isMain, $options);
                } catch (ex) {
                    if (ex.code !== "MODULE_NOT_FOUND") {
                        throw ex;
                    }
                    let isNative = false;
                    try {
                        isNative = require.resolve.paths($request) === null;
                    } catch (ex) {
                        debug("fail to identify module: " + $request);
                        isNative = false;
                    }
                    if (!isNative) {
                        if (!$request.startsWith(snapshotPath)) {
                            if ($request.startsWith("./") && $parent && $parent.filename) {
                                $request = normalizePath(path.resolve(path.dirname($parent.filename), $request));
                            } else {
                                $request = normalizePath(path.resolve(snapshotPath + "/node_modules/", $request));
                            }
                        }
                        debug("module: " + $request);
                    }
                    const savePathCache = nativeModule._pathCache;
                    nativeModule._pathCache = Object.create(null);
                    try {
                        filename = _resolveFilename($request, $parent, $isMain, $options);
                    } finally {
                        nativeModule._pathCache = savePathCache;
                    }
                }
                return filename;
            };

            if (!process.env.hasOwnProperty("NODEJSRE_SKIP_LOADER")) {
                process.argv.splice(1, 0, snapshotPath + "/resource/javascript/loader.min.js");
            }
            if (process.env.hasOwnProperty("NODEJSRE_NODE_PATH")) {
                process.env.NODE_PATH = process.env.NODEJSRE_NODE_PATH;
            }
        }
    },

    destroy: function () {
        debug("Destroying cached resources");
        loader.clearCachedResources();
    }
};
