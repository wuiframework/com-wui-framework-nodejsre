/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

"use strict";

module.exports = {
    runTestSuite: function ($suiteName, $tests) {
        const fontColor = "\x1b[34m";

        console.log(fontColor +"%s\x1b[0m", "Running test suite '" + $suiteName + "'");

        for (let i = 0; i < $tests.length; ++i) {
            runTest($tests[i]);
        }

        console.log(fontColor +"%s\x1b[0m", "Ending test suite '" + $suiteName + "'");
    }
};

function runTest($testFunction) {
    console.log(" - Running test '" + $testFunction.name + "'");

    $testFunction();

    console.log(" - Ending test '" + $testFunction.name + "'");
}
