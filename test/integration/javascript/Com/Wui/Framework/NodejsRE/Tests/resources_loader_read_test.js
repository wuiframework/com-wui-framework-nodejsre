/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

"use strict";

const assert = require("assert").strict;
const path = require("path");

const fixture = require("./Helper/fixture");
const logger = require("./Helper/logger");
const resourcesLoader = require("resources_loader");

module.exports = {
    run: function () {
        fixture.runTestSuite(path.basename(__filename), [
            testReadWithIncorrectParamLength,
            testReadWithIncorrectParamTypes,
            testReadWithIncorrectParams]);
    }
};

function testReadWithIncorrectParamLength() {
    const incorrectCountOfArgsError = {
        name: "SyntaxError",
        message: "Incorrect count of arguments, 3 or 4 expected"
    };

    assert.throws(
        () => {
            resourcesLoader.read(null);
        },
        incorrectCountOfArgsError
    );

    assert.throws(
        () => {
            resourcesLoader.read("", "", 8, null, undefined, undefined);
        },
        incorrectCountOfArgsError
    );
}

function testReadWithIncorrectParamTypes() {
    const incorrectTypeOfArgError = {
        name: "TypeError",
        message: "Group, id, path have to be strings"
    };

    assert.throws(
        () => {
            resourcesLoader.read(null, "ID", "path");
        },
        incorrectTypeOfArgError
    );

    assert.throws(
        () => {
            resourcesLoader.read("some-group", 999, "path");
        },
        incorrectTypeOfArgError
    );
}

function testReadWithIncorrectParams() {
    assert.throws(
        () => {
            resourcesLoader.read("foo", "boo", "./__non-existing_file", logger.logCallback);
        },
        {
            name: "Error",
            message: "Could not open file ./__non-existing_file for reading"
        }
    );

    assert.throws(
        () => {
            resourcesLoader.read("some-group", "some-id", __filename, logger.logCallback);
        },
        {
            name: "Error",
            message: "Failed to validate the magic sequence"
        }
    );
}
