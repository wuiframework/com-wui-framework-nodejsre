/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

"use strict";

const assert = require("assert").strict;
const path = require("path");

const fixture = require("./Helper/fixture");
const logger = require("./Helper/logger");
const resourcesLoader = require("resources_loader");

module.exports = {
    run: function () {
        fixture.runTestSuite(path.basename(__filename), [
            testWriteWithIncorrectParamLength,
            testWriteWithIncorrectParamTypes,
            testWriteWithIncorrectParams]);
    }
};

function testWriteWithIncorrectParamLength() {
    const incorrectCountOfArgsError = {
        name: "SyntaxError",
        message: "Incorrect count of arguments, 4 or 5 expected"
    };

    assert.throws(
        () => {
            resourcesLoader.write(null);
        },
        incorrectCountOfArgsError
    );

    assert.throws(
        () => {
            resourcesLoader.write("", "", 8, null, undefined, undefined);
        },
        incorrectCountOfArgsError
    );
}

function testWriteWithIncorrectParamTypes() {
    const incorrectTypeOfArgError = {
        name: "TypeError",
        message: "Group, id, path, path-to-resource have to be strings"
    };

    assert.throws(
        () => {
            resourcesLoader.write(null, "ID", "path", "path-to-resource");
        },
        incorrectTypeOfArgError
    );

    assert.throws(
        () => {
            resourcesLoader.write("some-group", 999, "path", "path-to-resource", null);
        },
        incorrectTypeOfArgError
    );
}

function testWriteWithIncorrectParams() {
    assert.throws(
        () => {
            resourcesLoader.write("foo", "boo", "./__non-existing_file", "path-to-resource", logger.logCallback);
        },
        {
            name: "Error",
            message: "File path-to-resource cannot be opened for reading"
        }
    );
}
