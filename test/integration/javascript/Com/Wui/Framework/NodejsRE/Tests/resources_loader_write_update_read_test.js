/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

"use strict";

const assert = require("assert").strict;
const fs = require("fs");
const path = require("path");
const process = require("process");

const fixture = require("./Helper/fixture");
const logger = require("./Helper/logger");
const resourcesLoader = require("resources_loader");

const sourceBinary = process.argv[0];
const destinationBinary = "node-for-testing";

module.exports = {
    run: function () {
        fixture.runTestSuite(path.basename(__filename), [
            testWriteAndReadSimple,
            testWriteAndReadComplex]);
    }
};

function setUp() {
    fs.copyFileSync(sourceBinary, destinationBinary);
    fs.chmodSync(destinationBinary, "777");
}

function tearDown() {
    checkArtifacts();

    fs.unlinkSync(destinationBinary);
}

function checkArtifacts() {
    if (fs.statSync(destinationBinary).size <= fs.statSync(sourceBinary).size) {
        throw Error(destinationBinary + " should have bigger size than " + sourceBinary);
    }
}

function testWriteAndReadSimple() {
    setUp();

    try {
        resourcesLoader.write("TEST", "script", destinationBinary, __filename, logger.logCallback);
        assert.strictEqual(Buffer.compare(resourcesLoader.read("TEST", "script", destinationBinary, logger.logCallback), fs.readFileSync(__filename)), 0);
    } catch (error) {
        assert.fail("Received error: " + error);
    } finally {
        tearDown();
    }
}

function testWriteAndReadComplex() {
    setUp();

    try {
        resourcesLoader.write("TEST", "script", destinationBinary, __filename, logger.logCallback);
        resourcesLoader.write("TEST", "binary", destinationBinary, __filename, logger.logCallback);

        assert.strictEqual(Buffer.compare(resourcesLoader.read("TEST", "script", destinationBinary, logger.logCallback), fs.readFileSync(__filename)), 0);
        assert.strictEqual(Buffer.compare(resourcesLoader.read("TEST", "binary", destinationBinary, logger.logCallback), fs.readFileSync(__filename)), 0);

        const packageConfPath = path.join(__dirname, '..', '..', '..', '..', '..', '..', '..', '..', 'package.conf.json');

        resourcesLoader.update("TEST", "script", destinationBinary, packageConfPath, logger.logCallback);
        assert.strictEqual(Buffer.compare(resourcesLoader.read("TEST", "script", destinationBinary, logger.logCallback), fs.readFileSync(packageConfPath)), 0);

        resourcesLoader.updateOrWrite("TEST", "newResource", destinationBinary, packageConfPath, logger.logCallback);
        assert.strictEqual(Buffer.compare(resourcesLoader.read("TEST", "newResource", destinationBinary, logger.logCallback), fs.readFileSync(packageConfPath)), 0);
        // check previous resources
        assert.strictEqual(Buffer.compare(resourcesLoader.read("TEST", "binary", destinationBinary, logger.logCallback), fs.readFileSync(__filename)), 0);
    } catch (error) {
        assert.fail("Received error: " + error);
    } finally {
        tearDown();
    }
}
