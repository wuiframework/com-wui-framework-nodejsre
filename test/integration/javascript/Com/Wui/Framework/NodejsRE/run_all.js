/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

"use strict";

const resourcesLoaderReadTest = require("./Tests/resources_loader_read_test");
const resourcesLoaderWriteTest = require("./Tests/resources_loader_write_test");
const resourcesLoaderWriteAndReadTest = require("./Tests/resources_loader_write_update_read_test");

resourcesLoaderReadTest.run();
resourcesLoaderWriteTest.run();
resourcesLoaderWriteAndReadTest.run();
